using System;
using System.Diagnostics;
using Xunit;

namespace Bastion.Cryptography.UnitTest
{
    public class UnitTest
    {
        [Fact]
        public void GenerateKeyLengthTest()
        {
            var keys = SecretProcessor.GenerateKeys();
            Assert.Equal(72, keys.publicKeyString.Length);
            Assert.Equal(64, keys.privateKeyString.Length);
        }

        [Fact]
        public void SignAndValidateTest()
        {
            var (publicKeyString, privateKeyString) = SecretProcessor.GenerateKeys();
            string data = "Hedgehogs are fast and smart";
            Debug.WriteLine("will sign the message: " + data);
            var signature = SecretProcessor.Sign(data, publicKeyString, privateKeyString);
            Debug.WriteLine("Signature: " + signature);

            Debug.WriteLine("Verifying Signature");
            Assert.True(SecretProcessor.Verify(data, signature, publicKeyString));
        }
    }
}