﻿using System;
using System.Text;
using Org.BouncyCastle.Asn1.Pkcs;
using Org.BouncyCastle.Asn1.X509;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.Pkcs;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.X509;

namespace Bastion.Cryptography
{
    public static class SecretProcessor
    {
        private static readonly int strength = 384;
        //private static readonly string algo = "SHA256withRSA";
        private static readonly string algo = "SHA1withRSA";
        private static readonly string separator = "==";


        public static (string publicKeyString, string privateKeyString) GenerateKeys()
        {
            RsaKeyPairGenerator generator = new RsaKeyPairGenerator();
            generator.Init(new KeyGenerationParameters(new SecureRandom(), strength));
            var pair = generator.GenerateKeyPair();

            SubjectPublicKeyInfo publicKeyInfo = SubjectPublicKeyInfoFactory.CreateSubjectPublicKeyInfo(pair.Public);
            byte[] serializedPublicBytes = publicKeyInfo.ToAsn1Object().GetDerEncoded();
            string serializedPublic = Convert.ToBase64String(serializedPublicBytes);
            PrivateKeyInfo privateKeyInfo = PrivateKeyInfoFactory.CreatePrivateKeyInfo(pair.Private);
            byte[] serializedPrivateBytes = privateKeyInfo.ToAsn1Object().GetDerEncoded();
            string serializedPrivate = Convert.ToBase64String(serializedPrivateBytes);

            // TODO: optimize code
            // QUESTION: how to get publicKey and privateKey directly from "pair" ?? 
            //  I don't like this serialization / deserialization step
            //  
            RsaKeyParameters publicKey = (RsaKeyParameters)PublicKeyFactory.CreateKey(Convert.FromBase64String(serializedPublic));
            RsaPrivateCrtKeyParameters privateKey = (RsaPrivateCrtKeyParameters)PrivateKeyFactory.CreateKey(Convert.FromBase64String(serializedPrivate));

            //string privateModulusBase64 = Convert.ToBase64String(privateKey.Modulus.ToByteArray());
            string privateExponentBase64 = Convert.ToBase64String(privateKey.Exponent.ToByteArray());
            string publicModulusBase64 = Convert.ToBase64String(publicKey.Modulus.ToByteArray());
            string publicExponentBase64 = Convert.ToBase64String(publicKey.Exponent.ToByteArray());

            string secretKeyString = privateExponentBase64;
            string publicKeyString = publicModulusBase64 + publicExponentBase64;

            return (publicKeyString, secretKeyString);
        }

        private static (string publicModulusBase64, string publicExponentBase64) GetModulisAndExponentFromPublicKeyString(string publicKeyString)
        {
            int idx = publicKeyString.IndexOf(separator);
            if (idx == -1)
                throw new ArgumentException(publicKeyString + " is not valid publicKeyString");
            string publicModulusBase64 = publicKeyString.Substring(0, idx + separator.Length);
            string publicExponentBase64 = publicKeyString.Substring(idx + separator.Length);
            return (publicModulusBase64, publicExponentBase64);
        }
        private static string GetModulis(string publicKeyString)
        {
            return GetModulisAndExponentFromPublicKeyString(publicKeyString).publicModulusBase64;
        }

        private static RsaKeyParameters MakeKey(string modulusBase64, string exponentBase64, bool isPrivateKey)
        {
            var modulus = new BigInteger(Convert.FromBase64String(modulusBase64));
            var exponent = new BigInteger(Convert.FromBase64String(exponentBase64));
            return new RsaKeyParameters(isPrivateKey, modulus, exponent);
        }

        public static string Sign(string data, string publicKeyString, string secretKeyString)
        {
            string privateModulusBase64 = GetModulis(publicKeyString); // public and private modulus are same
            
            // Make the key 
            RsaKeyParameters key = MakeKey(privateModulusBase64, secretKeyString, true);

            ISigner signer = SignerUtilities.GetSigner(algo);

            // Populate key 
            signer.Init(true, key);

            // Get the bytes to be signed from the string 
            var bytes = Encoding.UTF8.GetBytes(data);

            // Calc the signature
            signer.BlockUpdate(bytes, 0, bytes.Length);
            byte[] signature = signer.GenerateSignature();

            // Base 64 encode the sig so its 8-bit clean 
            var signedString = Convert.ToBase64String(signature);

            return signedString;
        }

        public static bool Verify(string data, string expectedSignature, string publicKeyString)
        {
            // parse public key
            var (PublicModulusBase64, PublicExponentBase64) = GetModulisAndExponentFromPublicKeyString(publicKeyString);

            // Make the key 
            RsaKeyParameters key = MakeKey(PublicModulusBase64, PublicExponentBase64, false);

            // Init alg 
            ISigner signer = SignerUtilities.GetSigner(algo);

            // Populate key 
            signer.Init(false, key);

            // Get the signature into bytes 
            var expectedSig = Convert.FromBase64String(expectedSignature);

            // Get the bytes to be signed from the string 
            var msgBytes = Encoding.UTF8.GetBytes(data);

            // Calculate the signature and see if it matches 
            signer.BlockUpdate(msgBytes, 0, msgBytes.Length);
            return signer.VerifySignature(expectedSig);
        }
    }
}
